function createGerms(elem, count = 100, large = false) {
    const spotCount = count;
    for (let i = 0; i < spotCount; i++) {
        let spot = document.createElement("div");
        let dimensions =  randomPair();
        spot.setAttribute('id', elem + '-germ-' + (i + 1));
        spot.setAttribute("class", 'germ-spot germ-' + (i + 1) + ' float-' + Math.ceil(rndNum(1, 8)));
        spot.style.height=(large ? (dimensions[0] * 2) + 'px' : dimensions[0] + 'px');
        spot.style.width=(large ? (dimensions[1] * 2) + 'px' : dimensions[1] + 'px');
        spot.style.left=(rndNum(10, 90) + '%');
        spot.style.top=(rndNum(4, 93) + '%');
        if (trueFalse()) {
        spot.style.border='2px solid #79E6B1';
        trueFalse() ? spot.style.background = '#79E6B1' : spot.style.background = 'transparent' ;
        }
        trueFalse() ? spot.style.background = '#79E6B1' : spot.style.background = 'transparent' ;
        document.getElementById(elem).appendChild(spot);
    }
}

//create clone of first petrie to reduce js overhead
function cloneNode (original) {
    let itm = document.getElementById(original), cln = itm.cloneNode(true);
    cln.setAttribute('id', 'petrie-2');
    document.getElementById("germs").appendChild(cln);
}
function randomPair(min, max) {
    let x = Math.ceil(rndNum(2, 13));
    let y = x;
    return [x, y] //returns same randaom pair to ensure symetrical shaped circle (same H & W)
}
function rndNum(min, max) {
    return Math.random() * (max - min) + min;
}

function trueFalse() {
    return Math.random() <= 0.4 //lower integer here increase the chances of a truthy
}

function activate(element, anchorTop, anchorBot) {
    if (anchorTop <= 500 && anchorBot >= 50) {
        element.classList.add('animate');
        if (!document.getElementById('petrie-3').hasChildNodes()) {
            bottleGerms('petrie-3', 20, true);//create germs: location, count, spot size Large
        }
    } else {
        element.classList.remove('animate');
    }
}
//window Load
window.addEventListener('load', this.createGerms('petrie-1', 25, false));
window.addEventListener('load', this.cloneNode('petrie-1'));

window.addEventListener('scroll', function (event) {
    let elemArray = ['hero-1','hero-2', 'hero-3', 'hero-4'], 
        element;
        for (let i=0; i<elemArray.length; i++) {
            element = document.getElementById(elemArray[i])
            elemRect = element.getBoundingClientRect(),
            heroAnchorTop   = elemRect.top,
            heroAnchorBot   = elemRect.bottom
            activate(element, heroAnchorTop, heroAnchorBot);
        }
}, false);


function bottleGerms(elem, count = 10, large = true) {
    const spotCount = count;
    for (let i = 0; i < spotCount; i++) {
        let spot = document.createElement("div");
        let dimensions =  randomPair();
        spot.setAttribute('id', elem + '-germ-' + (i + 1));
        spot.setAttribute("class", 'germ-spot germ-' + (i + 1) + ' float-' + Math.ceil(rndNum(1, 8)));
        spot.style.height=(large ? (dimensions[0] * 3) + 'px' : dimensions[0] + 'px');
        spot.style.width=(large ? (dimensions[1] * 3) + 'px' : dimensions[1] + 'px');
        spot.style.left=(rndNum(0, 100) + '%');
        spot.style.top=(rndNum(8, 80) + '%');
        if (trueFalse()) {
        spot.style.border='2px solid #79E6B1';
        trueFalse() ? spot.style.background = '#79E6B1' : spot.style.background = 'transparent' ;
        }
        trueFalse() ? spot.style.background = '#79E6B1' : spot.style.background = 'transparent' ;
        document.getElementById(elem).appendChild(spot);
    }
}